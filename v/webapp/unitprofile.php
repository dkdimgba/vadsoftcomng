<?php 
// session_name("performance-org");
// session_start();
// include('functions.php');
if ($_GET) {
	$id = $_GET['id'] ;
	require("unitppt.php");
	// $name = $id; // $_GET['name'];
}
?>


<?php
if ($_GET['mode'] == "summarylist") {
?>

      <?php
      // list assesmen types
      foreach (GetAssType($connection, $_SESSION['id']) as $row)
      {
        // echo "<h4>".$row['astyp_name'] ."</h4>";

        // fetch all kpis for this staff on each assesment type

        // kpi count
        $kpi_count = 0;
        // get kpi of attached to staff alone
        foreach (GetKPIForStaff($connection,$name,$row['astyp_id']) as $kpi_staff){
          // get the kpi title
          foreach (GetKPI($connection,$kpi_staff['asp_kpi']) as $kpi)
          {
            $kid = $kpi_staff['asp_kpi'];
            // link
            $randvalue = md5(rand(0000,9999)).md5(rand(0000,9999)).md5($kid);
            echo "<div class='col-md-6' style='padding:2px;'>
                    <div class='box box-success'>
                      <div class='box-header with-border'>
                          <h3 class='box-title'>KPI: <a href='#'>".$kpi['kpi_name']."</a></h3>
                      </div>
                      <div>";
                        echo "<table class='table table-condensed' style='font-size:13px;'><tbody>";
                        // get cummulative value of KPI for current year
                        GetStaffTestAllss($connection,$id,$type='staff',$kid,'summarylist');
                        echo "<tr><td colspan='2'> <a href='staffprofile?tag=$randvalue&id=$name&measure=historylist'>See History</a> </td></tr>";
                        echo "</tbody></table>";
                      echo "</div>
                    </div>
            </div>";

            // increase count
            $kpi_count++;
          }

        }

      }

      ?>

<?php } elseif($_GET['mode'] == "historylist"){ ?>

      
      <?php

      // list assesmen types
      foreach (GetAssType($connection) as $row)
      {
        // echo "<h4>".$row['astyp_name'] ."</h4>";
// 
       // echo $name;
        // fetch all kpis for this staff on each assesment type

        // kpi count
        $kpi_count = 0;
        // get kpi of attached to staff alone
        foreach (GetKPIForStaff($connection,$name,$row['astyp_id']) as $kpi_staff){
          // get the kpi title
          foreach (GetKPI($connection,$kpi_staff['asp_kpi']) as $kpi)
          {
            $kid = $kpi_staff['asp_kpi'];
            // link
            $randvalue = md5(rand(0000,9999)).md5(rand(0000,9999)).md5($kid);
            echo "<div>
                    <div class='box box-success'>
                      <div class='box-header with-border'>
                          <h3 class='box-title'>KPI: <a href='#'>".$kpi['kpi_name']."</a></h3>
                      </div>
                      <div>";
                        echo "<table class='table table-condensed' id='example1'>";
                        echo "<thead>
                                <tr>
                                  <th>Date</th>
                                  <th>Parameter</th>
                                  <th>Value</th>
                                  <th>IPP</th>
                                  <th>OPP</th>
                                </tr>
                        </thead>
                        <tbody>";

                        // get cummulative value of KPI for current year
                        GetStaffKPIHistory($connection,$id,$type='staff',$kid,'historylist');
                        // echo "<tr><td colspan='2'> <a href='staffprofile?tag=$randvalue&id=1&measure=summarylist'>See History</a> </td></tr>";
                        echo "</tbody></table>";
                      echo "</div>
                    </div>
            </div>";

            // increase count
            $kpi_count++;
          }

        }

      }

      ?>



<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Month', 'Efficiency', 'Effectiveness'],
          ['2004',  1000,      400],
          ['2005',  1170,      460],
          ['2006',  660,       1120],
          ['2007',  1030,      540]
        ]);

        var options = {
          title: 'Staff Performance',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart.draw(data, options);
      }
    </script>
<!--<div id="curve_chart" style="width: 100%; height: 500px"></div>-->

<!--<canvas id="myChart" width="400" height="400"></canvas>-->
<?php } ?>