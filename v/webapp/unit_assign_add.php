<?php 
include('functions.php');

if($_POST) 
{ 
    $staff = $_POST['assign_unit_staff'];
    $unit = $_POST['unit'];
    $linkperson = $_POST['linkperson'];

    // first of count staff selected
    $staff_count = count($staff);

    if ($staff_count > 1)
    {
        // check if link person is yes
        if ($linkperson == "yes")
        {
            // throw an error message
            echo "Operation aborted. Check and retry";
            exit();
        }
        else
        {
            $assign_unit_now = true;
        }
    }
    else
    {
        $assign_unit_now = true;
    }


    // save record
    if ($assign_unit_now == true)
    {
        if($linkperson == "yes")
        {
            $linkperson = 1;
        }
        
        // print_r($staff);
        foreach ($staff as $key => $value) 
        {
            // die(print_r($value)) ;
            // array_push($shiftarraycalc,$value);
            if (AssignUnit($connection,$value,$unit,$linkperson,$datetime) == true) 
            {
                // header("Refresh:1; url=../assignunit");
                // registration successfully  
                echo "Operation Was Successful";
            }
            else
            {
                echo "Operation was Unsuccessful";
            }
        }

    }

}
else
{
    echo "Unsuccessful";
}