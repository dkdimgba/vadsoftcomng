<?php 
// start session
session_name("performance-org");
session_start();
// include the function page
include('functions.php');

echo $_SESSION['id'];

// Register Users
if($_POST) 
{
    // get the action
    $action = $_GET['action'] ;

    // swiych actions using the if..else if logic
    if ($action == "asstyp") 
    {
        $name = $_POST['name'];
        $discrip = $_POST['discrip'];
        $cat = $_POST['cat'];
        $level = $_POST['org_level']; 
     
        if (InsertParamType($connection,$name,$discrip,$cat,$datetime,$level,$_SESSION['id']) == true) 
        {
            header("Refresh:0; url=../assesmenttype");  
            echo "Registration Was Successful";
        } 
        else 
        {
            echo "Registration was Unsuccessful";
        }
    }
    
    elseif ($action == "paramcat") 
    {
        $name = $_POST['name'];
        $discrip = $_POST['discrip'];
        $cat = $_POST['type'];
        $typ = $_POST['typ'];
        $typss = $_POST['typss'];
        $kpi = $_POST['kpi'];
        // die($typ);
        $count = 0;
        // find the org_grp
        $typss = getAssTypeGroup($connection, $cat);

        if(isset($kpi)) 
        {  
            // Check if selections were made
            // print_r($staff);
            foreach ($kpi as $key => $value) 
            {
                // echo ($value)."<br>" ;
                // array_push($shiftarraycalc,$value);
                // if (AssignUnit($connection,$value,$unit,$datetime) == true) 
                // {
                //     header("Refresh:1; url=../assignunit");
                //     // registration successfully  
                //     echo "Registration Was Successful";
                // }
                // else
                // {
                //     echo "Registration was Unsuccessful";
                // }

                if (InsertAssParamCat($connection,$name,$discrip,$cat,$datetime,$typss,$value,$_SESSION['id']) == true) 
                {
                    $count = $count + 1;
                }
                else
                {
                    $count = 0;
                }

            }
        }

        if ($count>0)
        {
            header("Refresh:0; url=../assesmentmeasure");  
            echo "Registration Was Successful";
        } 
        else
        {
            echo "Registration was Unsuccessful";
        }
    }
    
    // elseif ($action == "pers") 
    // {

    //     $name = $_POST['name'];
    //     $discrip = $_POST['discrip'];
    //     // die($typ);
     
    //     if (InsertPersonality($connection,$name,$discrip,$datetime) == true) 
    //     {
    //         header("Refresh:0; url=personality.php");  
    //         echo "Registration Was Successful";
    //     }
    //     else
    //     {
    //         echo "Registration was Unsuccessful";
    //     }
 
    // }

    elseif ($action == "params") 
    {
        if (isset($_POST['dept'])) 
        {
            $dept = $_POST['dept']; $unit = ""; $staff = ""; $mode = 4;
            // get staff id
            $staff_id = GetLinkPersonDept($connection, $dept);
            // get department name
            $dept_name = GetDeptName($connection, $dept);
            // type
            $type = "your department " . $dept_name;
        }
        elseif(isset($_POST['unit']))
        {
            $unit = $_POST['unit']; $dept = ""; $staff = ""; $mode = 1;
            // get link person staff id
            $staff_id = GetLinkPersonUnit($connection, $unit);
            // get the unit name
            $unit_name = GetUnitName($connection, $unit);
            // type
            echo "i am unit";
            $type = "your unit " . $unit_name;
        }
        elseif(isset($_POST['staff']))
        {
            $staff = $_POST['staff']; $dept = ""; $unit = ""; $mode = 3;
            // staff id
            $staff_id = $staff;
            // type
            $type = "you";
        }

        $name = $_POST['name'];
        $discrip = $_POST['discrip'];
        $cat = $_POST['type'];
        $form = $_POST['formtyp'];
        $value = $_POST['value'];
        $kpi = $_POST['kpi'];
        $typ = $_POST['typ'];
        $lv = $_POST['orglv'];
        $cattype = $_POST['cattype'];
             
        if (InsertAssParam($connection,$name,$discrip,$cat,$datetime,$form,$value,$kpi,$dept,$unit,$staff,$typ,$lv,'',$cattype,$_SESSION['id']) == true) 
        {
            // update the linksent date
            $time = time();
            $datetimelinksent = date("Y-m-d h:i:s", $time);

            $kpi_measure = GetKPIId($connection, $kpi);
            // send email to personeel
            // notifying him/her that assesment parameters have been logged
            // and also when to expect the first measurement link
            // get email of receiver
            // find staff email
            $staff_email = GetStaffEmail($connection,$staff_id);
            // find staff name
            $staff_name = GetStaffName($connection,$staff_id);

            // format link
            // $measure_link = "http://vadsoft.com.ng/app/measurement?name=$staff_id&m_staff=listkpimeasurecategory&kid=$kpi_id;";
            // format email body | subject
            $message = "Hello $staff_name, an assesment template has been generated for $type. It is to measure your Value Added on the Key Performance Index for $kpi_measure\n\nRegards";

            // send email to user
            mail($staff_email, "VADSoft Assesment Template", $message, "From : info@vadsoft.com.ng");

            //  // update the PI table with last time stamp link was sent
            UpdateKPILastTimeLinkSent($connection, $kpi, $_SESSION['id'], $datetimelinksent);

            header("Refresh:0; url=../assesmentvaluesetup?unit=$mode");  
            echo "Registration Was Successful";
        }
        else
        {
            echo "Registration was Unsuccessful";
        }
    }

    elseif ($action == "kpi") 
    {

        $name = $_POST['name'];
        $discrip = $_POST['discrip'];
        $routine = $_POST['routine'];
     
        if (InsertKPI($connection,$name,$discrip,$routine,$datetime,$_SESSION['id']) == true) 
        {
            // automatically send link to staff
            // do this as a cron script on its own
            
            header("Refresh:0; url=../assesmentkpi");  
            echo "Registration Was Successful";
        } 
        else 
        {
            echo "Registration was Unsuccessful";
        }
    }
}
else
{
    echo "Access Denied";
}