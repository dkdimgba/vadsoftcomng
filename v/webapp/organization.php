<?php 
$get_organization = GetOrganizations($connection, $_SESSION['id']); 
foreach($get_organization as $row)
{
	$org_name = $row['org_name'] ;
    $org_adname = $row['org_admin'] ;
    $org_email = $row['org_email'] ;
    $org_address = $row['org_address'];
    $org_phone = $row['org_phone'];
    $org_location = GetOrganizationLocation($connection, $row['org_location']);
    $org_id = $row['org_id'];
    $org_category = $row['org_category'];
}
?>

<?php 

$mode = $_GET['mode'];

if ($mode == "edit")
{
	// include file for edit and update
	include("organization_edit.php"); 
}

else

{
	// include file for edit and update
	include("organization_new.php"); 
}


?>