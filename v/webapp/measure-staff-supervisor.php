<?php 
error_reporting(E_ALL);
session_name("performance-org");
session_start();

require_once("functions.php");

// decode the url anchor value
$ref = base64_decode($_GET['ref']);
echo $ref;
// explode using the underscore(_) seperator
$ref = explode("_", $ref);
// // extract the values from the array
$action = $ref[1]; // value for action. either decline or accept
$name = $ref[0]; // this is the id of teh staff, department or unit

// echo "<br>".$action;

if ($action == "decline")
{
	// echo "This request have been declined";
	$aid = $ref[2]; // this is the id of the measurement parameter and template
	// updates the db table
	if ( DeclineAssessStaffInput($connection, $aid) == true)
	{
		echo "Request has been declined";
		// send email to staff that measurement was refused by supervisor
	}
}
else
{
	// fetch action value
	if ($_POST) 
	{
		// Get the value of the assessment parameter 
		// run a loop to get the values 
		// insert the values into the assessment table 

		$score = $_POST['score'];
		$param = $_POST['param'];
		$name = $_GET['name'];
		$typ = $_POST['typ'];
		$type = $_POST['type'];
		$standard = $_POST['standard'];
		$cat = $_POST['cat'];
		$exp_hours = $_POST['exp_hours'];
		$hours = $_POST['hours'];
		$days = $_POST['days'];
		$exp_days = $_POST['exp_days'];
		$dept =  $_GET['dept'];
		$usr_typ = "staff";
		$kpi = $_POST['kpi'];
		$kpi_routine = $_POST['kpi_routine'];
		$param = $_POST['param'];
		$aid = $_POST['aid'];
		$labour = 1;

		// echo "<br>". $aid;
		foreach( $score as $index => $code ) 
		{
			// calculate value
	        $input_per_person_unit = $standard[$index] / $labour * $exp_hours * $exp_days;
			$output_per_person_unit = $score[$index] / $labour * $hours * $days;
			$value = $input_per_person_unit / $output_per_person_unit;

			// lets get the category we are to measure
	  		$measure_category = GetCatNameId($connection,$cat[$index]);
			if (strtolower($measure_category) == strtolower("effectiveness")) 
			{
				// perform calculation
				$effectiveness = $value;
				// register effectiveness
				RecordAssessStaffResult($connection,$kpi,$aid,$datetime,$date,$effectiveness);
			} 
			elseif (strtolower($measure_category) == "efficiency"))
			{
				// perform calculation
				$efficiency = $value;
				// register efficiency
				RecordAssessStaffResult($connection,$kpi,$aid,$datetime,$date,$efficiency);
			}

			$ass_id = RecordAssessStaffInput($connection,$name,$param[$index],$code,$datetime,$typ[$index],$cat[$index],$usr_typ,$date,$type[$index],$standard[$index],$dept,$kpi,$kpi_routine,$aid);

			// // get staff supervisor
			// $staff_supervisor = GetStaffSupervisor($connection, $name);
			// // get supervisor email
			// $supervisor_email = GetStaffEmail($connection, $staff_supervisor);
			// // get supervisor email
			// $supervisor_name = GetStaffName($connection, $staff_supervisor);
			// // get staff name
			// $staff_name = GetStaffName($connection, $name);
			// // find kpi title
			// $kpi_title = GetKPIId($connection, $kpi);

			// // format link
			// $measure_link = "http://vadsoft.com.ng/app/handlemeasurement?name=$name&kid=$kpi&m_staff=measurestaffkpi&aid=$ass_id";

			// // format email subject
			// $email_subject = $staff_name . " effectiveness report on " . $kpi_title . " for January 2017";
			// // format email message
			// $email_message = "Hello ". $supervisor_name . "\n\nKindly find attached staff report for the period as stated in the subject. Click on the link below to access the staff ". $measure_link . "\n\nKind Regards";

			// // echo $email_subject."<br>".$email_message;

			// // send email to staff supervisor / organisation
			// mail($supervisor_email, $email_subject, $email_message, "info@vadsoft.com.ng"); 
		}		
		

		echo "Successful";
		// header("Location: ../../staffmeasurement?name=$name&kid=$kpi&m_staff=showreport");
		// echo $_GET['as'];	

		echo "<script>";
		echo "window.location.replace('../../handlemeasurement?name=$name&kid=$kpi&m_staff=showreport')";
		echo "</script>";
	}
}
?>