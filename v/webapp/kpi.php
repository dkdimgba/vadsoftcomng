<!-- list kpis -->
<?php
	$get_kpi = GetKPI($connection, '', $_SESSION['id']);
?>
		<div class="">
			<table class="table table-striped" id="">
		        
		        <thead>
					<tr>
						<th>S/N</th>
						<th>Partner</th>
						<th>Date</th>
					</tr>
		        </thead>

		        <tbody>
				<?php 
					$sn=1; 
					foreach($get_kpi as $row) 
					{ 
						$dtime = date("jS F, Y",strtotime($row['registered_at']));// DateTime::createFromFormat("m-Y", $row['registered_at']);
						$name = $row['kpi_name'] ;
                     	// $discrip = $row['kpi_discrip'] ;
                    	// $id = $row['kpi_id'] ;
				?>
					<tr>
						<th scope="row"><?= $sn ?></th>
						<td><a href="#"><b><?php echo $name ?></b></a></td>
						<td><?php echo $dtime ?></td>
					</tr>
				<?php 
						$sn++; 
					} 
				?>
				</tbody>
			</table>
		</div>