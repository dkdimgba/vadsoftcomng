<?php 
session_name("performance-org");
session_start();
// Register Users
if($_POST) 
{ 
    include('functions.php');
    $fname          = $_POST['fname'];
    $lname          = $_POST['lname'];
    $dept           = $_POST['dept'];
    $linkperson     = $_POST['linkperson'];
    // $qualification = $_POST['qualification'];
    // $add = $_POST['add']; 
    $sex            = $_POST['sex'];
    $email          = $_POST['email'];
    $phone          = $_POST['phone'];
    $snumber        = $_POST['snumber'];
    // location of assignment
    $state          = $_POST['state']; 
    $city           = $_POST['city'];

    $org            = $_SESSION['id'];

    $new_staff = InsertStaff($connection,$fname,$lname,$dept,$sex,$phone,$email,$org,$datetime,$snumber,$state,$city);
     
    if ($new_staff > 0) 
    {
        // update department by adding link person
        // retrieve the last user id entered to db
        if ($linkperson == 1)
        {
            AddLinkPersonToDepartment($connection, $new_staff, $dept, $org);
        }

        header("Refresh:1; url=../staff");  
        echo "Registration Was Successful";
    }
    else
    {
        header("Refresh:1; url=../staff");  
        echo "Registration was Unsuccessful";
    }
}
else
{
    header("Refresh:1; url=../staff"); 
    echo "Unsuccessful";
}
