<?php
include("functions.php");

// cron process to manage measurement process
// checks for active businesses first
$activeOrganizations = GetOrganizations($connection, '');

// var_dump($activeOrganizations);

// get individual Organizations
foreach ($activeOrganizations as $row) 
{
	$org_id = $row['org_id'];

	// retrieves the organisation's kpis
	$organizationKPI = GetKPI($connection, '', $org_id);

	// get all the assesment type connected to this company
	$organizationAssesmentType = GetAssType($connection, $org_id);

	// echo "<pre>";
	// var_dump($organizationKPI);
	// echo "</pre>";

	// loop through to pick each assesment type for the selected organisation
	foreach ($organizationAssesmentType as $assesment_row)
	{
		// assesment_type_id
		$assesment_type_id = $assesment_row['astyp_id'];
    
		// assessment_level
		$assessment_level = $assesment_row['astyp_org_level'];

		// echo $assessment_level;

		// loop through to fetch each kpi
		foreach ($organizationKPI as $kpi_row)
		{
			$kpi_id = $kpi_row['kpi_id'];

			// get the kpi routine
			$kpiRoutine = GetKPIRoutine($connection,$kpi_id);

			// can we get the last time this KPI was sent to for measurement
			$kpi_last_assesement_date = GetKPILastAssesmentDate($connection, $kpi_id);

			// $daysdiffernce = date_diff(date_create('2017-02-25'),date_create('2017-02-23'));
			// echo $daysdiffernce->format("%h");
			// echo $start;

			$start_time = strtotime(date($kpi_last_assesement_date));
			$end_time = strtotime(date("Y-m-d h:i:s", time()));

			$difference_date = $end_time - $start_time;
			$difference_hours = round($difference_date / 3600);

			// echo $difference_hours . "<br>";

			// $start_time = new DateTime($start_time); // start time
			// $end_time = new DateTime($end_time); // end time
			// $interval = $start_time->diff($end_time);    
			
			// $difference_days = $interval->format('%d');  // 00 years 0 months 0 days 08 hours 0 minutes 0 seconds
			// $months = $interval->format('%m'); 
			// $difference_hours = $interval->format('%h');

			// echo $difference_days ."-" . $difference_hours."<br>";

			// get the nature of this KPI 
			// department | Unit | staff
			// get the staff responsible to receive link for department and unit

			// can we find the last time this kpi was measured

			// get staffs attached to this kpi for this particular organisation 
			$staffOnKPI = GetKPIForStaff($connection, '', $assesment_type_id);

			foreach ($staffOnKPI as $staff_row)
			{
				$ass_key_id = rand(0000,9999);

				if ($assessment_level == "staff-ass")
				{
					echo "Staff";
					$staff_id = $staff_row['asp_staff'];
				}

				if ($assessment_level == "unit")
				{
					echo "Unit";
					// find the name of unit
					$unit_id = $staff_row['asp_unit'];
					// find the staff for whom the link will be sent to
					$staff_id = GetLinkPersonUnit($connection, $unit_id);
					// get unit name
					$unit_name = GetUnitName($connection, $unit_id);
				}

				if ($assessment_level == "department")
				{
					echo "Department";
					// find the name of unit
					$dept_id = $staff_row['asp_dept'];
					// find the staff for whom the link will be sent to
					$staff_id = GetLinkPersonDept($connection, $dept_id);
					// get department name
					$dept_name = GetDeptName($connection, $dept_id);
				}

				// find staff email
				$staff_email = GetStaffEmail($connection,$staff_id);
				// find staff name
				$staff_name = GetStaffName($connection,$staff_id);


				if ($assessment_level == "staff-ass")
				{
					// format link
					$measure_link = "http://vadsoft.com.ng/app/measurement?name=$staff_id&mode=listkpimeasurecategory&kid=$kpi_id&level=staff&akid=$ass_key_id";
					// format email body | subject
					$message = "Hello $staff_name, click on the link below to measure yourself ". $measure_link;
				}

				if ($assessment_level == "unit")
				{
					// format link
					$measure_link = "http://vadsoft.com.ng/app/measurement?name=$unit_id&mode=listkpimeasurecategory&kid=$kpi_id&level=unit";
					// format email body | subject
					$message = "Hello $staff_name, click on the link below to measure your unit $unit_name ". $measure_link;
				}

				if ($assessment_level == "department")
				{
					// format link
					$measure_link = "http://vadsoft.com.ng/app/measurement?name=$dept_id&mode=listkpimeasurecategory&kid=$kpi_id&level=department";
					// format email body | subject
					$message = "Hello $staff_name, click on the link below to measure your department $dept_name ". $measure_link;
				}

				if ($kpiRoutine == "daily")
				{
					// echo "Daily<br>";
					// echo $difference_hours."<br>";
					// check for last date difference
					// if ($difference_hours < 24)
					// {
						$time = time();
						$datetimelinksent = date("Y-m-d h:i:s", $time);
						// update the PI table with last time stamp link was sent
						// send email to user
						echo "Daily Measure<br>----------<br>";
						mail($staff_email, "VADSoft Measurement Link", $message, "info@vadsoft.com.ng");
						UpdateKPILastTimeLinkSent($connection, $kpi_id, $org_id, $datetimelinksent);
						echo "sent<br>$measure_link<br>$datetimelinksent";
					// }
				}

				if ($kpiRoutine == "weekly")
				{
					echo $difference_hours;
					// // check for last date difference
					// if ($difference_hours == 168)
					// {
						$time = time();
						$datetimelinksent = date("Y-m-d h:i:s", $time);
						mail($staff_email, "VADSoft Measurement Link", $message, "info@vadsoft.com.ng");
						// update the PI table with last time stamp link was sent
						UpdateKPILastTimeLinkSent($connection, $kpi_id, $org_id, $datetimelinksent);
						echo "sent<br>$measure_link<br>$datetimelinksent";
					// }
				}

				if ($kpiRoutine == "monthly")
				{
					echo $difference_hours;
					// // check for last date difference
					// if ($difference_hours == 672)
					// {
						$time = time();
						$datetimelinksent = date("Y-m-d h:i:s", $time);
						mail($staff_email, "VADSoft Measurement Link", $message, "info@vadsoft.com.ng");
						// update the PI table with last time stamp link was sent
						UpdateKPILastTimeLinkSent($connection, $kpi_id, $org_id, $datetimelinksent);
						echo "sent<br>$measure_link<br>$datetimelinksent";
					// }
				}
			}
		}
	}
}
