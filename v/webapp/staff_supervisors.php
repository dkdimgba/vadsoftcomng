
<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">Setup Supervisors</h3>
	</div>
	<div class="box-body">

		<form method="post" action="webapp/staff_perform_supervisor.php?mode=create">
			<label>Select Supervisor</label>
			<select id="supervisor[]" name="supervisor[]" class="form-control select2 input-lg" multiple>
				<option>Select Staff </option>
				<!-- <option value="200">All Category</option> -->
				<?php
				    $sql = "SELECT * FROM staff ORDER BY sta_fname" ;
				    $sql = $connection->query($sql) or die("Unsuccessful") ;
				    $sql ->setFetchMode(PDO::FETCH_ASSOC);
				    while($row = $sql->fetch())
				    {
				        ?>
				        <option value="<?php echo $row['sta_id']; ?>"><?php echo $row['sta_fname'] . " " . $row['sta_lname'];  ?></option>  
				        <?php
				    }
			  	?>
			</select>
			<br /><br />
			<br>
			<input type="submit" class="btn btn-success btn-lg" value="Save">
		</form>
	</div>
</div>
							
		
<?php $get_supervisors = GetSupervisors($connection, $_SESSION['id']) ;?> 


<div class="box box-success">
	<div class="box-header with-border">
		<h3 class="box-title">List of Supervisors</h3>
	</div>
	<div class="">	
		<table id="example1" class="table table-responsive">
			<thead>
				<tr>
					<th>S/N</th>
					<th>Supervisor</th>
					<th>No. of Staff</th>
				</tr>
			</thead>
			<tbody>
			<?php
				$sn = 1;
				foreach ($get_supervisors as $row)
				{
					$supervisor_name = GetStaffName($connection,$row['supervisor_staff_id']);
					echo "<tr>
					        <th scope='row'>$sn</th>
					        <td>$supervisor_name</td>
					        <td>0</td>
					     </tr>";
				}
			?>
			</tbody>
		</table>
	</div>
</div>