<?php 
include('functions.php');

if($_POST) 
{
    if ($_GET['mode']=="create")
    {
        $supervisor = $_POST['supervisor'];

        // print_r($staff);
        foreach ($supervisor as $key => $value) 
        {
            // die(print_r($value)) ;
            // array_push($shiftarraycalc,$value);
            $date = time();
            if (InsertSupervisor($connection,$value,$date) == true) 
            {
                header("Refresh:1; url=../createsupervisors");
                // registration successfully  
                echo "Registration Was Successful";
            }
            else
            {
                echo "Registration was Unsuccessful";
            }
        }
    }
    elseif ($_GET['mode']=="assign")
    {
        $type_value = $_POST['type_value'];
        $supervisor = $_POST['supervisor'];
        $type = $_POST['type'];

        // print_r($staff);
        foreach ($type_value as $key => $value) 
        {
            // die(print_r($value)) ;
            // array_push($shiftarraycalc,$value);
            $date = time();
            if (AssignSupervisor($connection,$type,$value,$supervisor,$date) == true) 
            {
                header("Refresh:1; url=../assignstafftosupervisors");
                // registration successfully  
                echo "Registration Was Successful";
            }
            else
            {
                echo "Registration was Unsuccessful";
            }
        }
    }

}
else
{
    echo "Unsuccessful";
}