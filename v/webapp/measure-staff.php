<?php 
error_reporting(E_ALL);
session_name("performance-org");
session_start();
if ($_POST) 
{
	// Get the value of the assessment parameter 
	// run a loop to get the values 
	// insert the values into the assessment table 
	require_once("functions.php");

	$score = $_POST['score'];
	$param = $_POST['param'];
	$name = $_GET['name'];
	$typ = $_POST['typ'];
	$type = $_POST['type'];
	$standard = $_POST['standard'];
	$cat = $_POST['cat'];
	$exp_hours = $_POST['exp_hours'];
	$hours = $_POST['hours'];
	$days = $_POST['days'];
	$exp_days = $_POST['exp_days'];
	$dept =  $_GET['dept'];
	$usr_typ = "staff";
	$kpi = $_POST['kpi'];
	$kpi_routine = $_POST['kpi_routine'];
	$param = $_POST['param'];

	// $labour = $_POST['labour'];

	$level = $_POST['level'];

	foreach($score as $index => $code) 
	{
		$ass_id = RecordAssessStaffInputForSupervisor($connection,$name,$param[$index],$code,$datetime,$typ[$index],$cat[$index],$usr_typ,$date,$type[$index],$standard[$index],$dept,$kpi,$kpi_routine);

		// get staff supervisor
		$staff_supervisor = GetStaffSupervisor($connection, $name);
		// get supervisor email
		$supervisor_email = GetStaffEmail($connection, $staff_supervisor);
		// get supervisor email
		$supervisor_name = GetStaffName($connection, $staff_supervisor);
		// get staff name
		$staff_name = GetStaffName($connection, $name);
		// find kpi title
		$kpi_title = GetKPIId($connection, $kpi);

		// get the kpi routine
		$kpiRoutine = GetKPIRoutine($connection,$kpi);
		// lets get the category we are to measure
   		$measure_category = GetCatNameId($connection,$cat[$index]);

        if ($kpiRoutine == "daily")
        {
        	$measure_time = date("D, d M Y", strtotime($kpi_routine));
        } 
        elseif ($kpiRroutine == "monthly") 
        {
        	$measure_time = date("M Y", strtotime($kpi_routine));
        }


		// format link
		$measure_link = "http://vadsoft.com.ng/app/handlemeasurement?name=$name&kid=$kpi&mode=measurestaffkpi&aid=$ass_id&mcategory=".strtolower($measure_category)."&level=$level";

		// format email subject
		$email_subject = $staff_name . " " . $measure_category ." report on " . $kpi_title . " ". $measure_time ;
		// format email message
		$email_message = "Hello ". $supervisor_name . "\n\nKindly find attached staff report for the period as stated in the subject. Click on the link below to access the staff ". $measure_link . "\n\nKind Regards";

		// echo $email_subject."<br>".$email_message."<br>".$email_link;

		// send email to staff supervisor / organisation
		mail($supervisor_email, $email_subject, $email_message, "From: info@vadsoft.com.ng"); 
	}		
	RecordUserHourSupervisor($connection,$name,$date,$hours,$exp_hours,$days,$exp_days,$ass_id,$kpi);

	echo "Successful";
	//header("Location: ../../staffmeasurement?name=$name&kid=$kpi&m_staff=showreport");
	// echo $_GET['as'];	

	echo "<script>";
	// echo "window.location.replace('../../staffmeasurement?name=$name&kid=$kpi&m_staff=showreport')";
	echo "</script>";
}
?>