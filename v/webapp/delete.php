<?php 
require("functions.php"); 

if ($_GET) {
	session_start();
	$obj = $_GET['obj'] ;
	if ($obj == 'orgcat') 
	{
		$mid = $_GET['id'] ;
		if (DeleteCat($connection,$mid) == true) 
		{
			header("Refresh:1; url=../organizationcategory");  
			echo "Category Was Successful Deleted";
		}
	}
	elseif ($obj == 'org') 
	{
		$cid = $_GET['id'] ;	
		if (DeleteOrg($connection,$cid) == true) 
		{
			header("Refresh:0; url=../organization") ;  
	     	echo " organization Was Successful Deleted" ;
		}
	}
	elseif ($obj == 'dept') 
	{
		$fid = $_GET['id'] ;
		if (DeleteDept($connection,$fid) == true) 
		{
			header("Refresh:1; url=../department");  
			echo "Department Was Successful Deleted";
		}
	}
	elseif ($obj =='unit') 
	{
		$cid = $_GET['id'];
		if (DeleteUnit($connection,$cid) == true) 
		{
			header("Refresh:1; url=../unit");  
			echo "Unit Was Successful Deleted";
		}
	}
	elseif ($obj =='staff') 
	{
		$cid = $_GET['id'] ;
		if (DeleteStaff($connection,$cid) == true) 
		{		
	    	header("Refresh:1; url=../staff");
	     	echo "Staff Was Successful Deleted";
		}
	}
}

?>