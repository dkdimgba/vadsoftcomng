<?php 
// session_name("performance-org");
// session_start();
// include('functions.php');
if ($_GET) {
	$id = $_GET['id'];
	require("stafppt.php");
	$name = $id; // $_GET['name'];
}


if ($_GET['measure'] == "summarylist") 
{
    echo "<div class='box box-success'>
                <div class='box-header with-border'>
                    <h3 class='box-title'>Staff List</h3>
                </div>
                <table class='table table-responsive'>
                        <thead>
                            <th>S/N</th>
                            <th>KPI</th>
                            <th>Routine</th>
                            <th>Date</th>
                        </thead>
                        <tbody>";

    // list assesmen types
    foreach (GetAssType($connection, $_SESSION['id']) as $row)
    {
        // echo "<h4>".$row['astyp_name'] ."</h4>";
        // fetch all kpis for this staff on each assesment type

        // kpi count
        $kpi_count = 1;
        // get kpi of attached to staff alone
        foreach (GetKPIForStaff($connection,$name,$row['astyp_id']) as $kpi_staff)
        {
            // get the kpi title
            foreach (GetKPI($connection,$kpi_staff['asp_kpi'], $_SESSION['id']) as $kpi)
            {
                $kid = $kpi_staff['asp_kpi']; // kpi id
                // $routine = $kpi_staff['asp_kpi_routine'];

                // link
                $randvalue = md5(rand(0000,9999)).md5(rand(0000,9999)).md5($kid);
                // echo "<div class='col-md-6' style='padding:2px;'>
                //         <div class='box box-success'>
                //             <div class='box-header with-border'>
                //                 <h3 class='box-title'>KPI: <a href='#'>".$kpi['kpi_name']."</a></h3>
                //             </div>
                //         <div>
                // </div>";

                // table to list all KPIs of staff
                echo "<tr>
                        <td>".$kpi_count."</td>
                        <td><strong><a href='staffprofile?tag=$randvalue&kid=$kid&id=$name&measure=historylist'>".$kpi['kpi_name']."</a></strong></td>
                        <td>".ucfirst($kpi['kpi_routine'])."</td>
                        <td>Date</td>
                    </tr>";

                // increase count
                $kpi_count++;
            }
        }
    }

    echo "</tbody>
        </table>
    </div>";

} 
elseif($_GET['measure'] == "historylist")
{
    $kid = $_GET['kid'];
    $ass_key_id = $_GET['akid'];
    
    // list assessment types
    // foreach (GetAssType($connection, $_SESSION['id']) as $row)
    // {
    //     // echo "<h4>".$row['astyp_name'] ."</h4>";
    //     // echo $name;
    //     // fetch all kpis for this staff on each assesment type

    //     // kpi count
    //     $kpi_count = 1;
    //     // get kpi of attached to staff alone
    //     foreach (GetKPIForStaff($connection,$name,$row['astyp_id']) as $kpi_staff)
    //     {
            // get the kpi title
            foreach (GetKPI($connection,$kid, $_SESSION['id']) as $kpi)
            {
                // $kid = $kpi_staff['asp_kpi'];
                // link
                // $randvalue = md5(rand(0000,9999)).md5(rand(0000,9999)).md5($kid);
                echo "<div>
                        <div class='box box-success'>
                            <div class='box-header with-border'>
                                <h3 class='box-title'>KPI: <a href='#'>".$kpi['kpi_name']."</a></h3>
                            </div>
                        <div>";
                        echo "<table class='table table-condensed' id='example1'>
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Effectiveness</th>
                                        <th>Efficiency</th>
                                        <th>Productivity Index</th>
                                    </tr>
                                </thead>
                                <tbody>";
                                    // get cummulative value of KPI for current year
                                    GetStaffKPIHistory($connection,$id,$kid,'historylist',$ass_key_id);
                                    // echo "<tr><td colspan='2'> <a href='staffprofile?tag=$randvalue&id=1&measure=summarylist'>See History</a> </td></tr>";
                                echo "</tbody>
                        </table>
                </div>";
                // increase count
                // $kpi_count++;
            }
    //     }
    // }
?>



<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(drawChart);

    function drawChart() {
        var data = google.visualization.arrayToDataTable([
          ['Month', 'Efficiency', 'Effectiveness'],
          ['2004',  1000,      400],
          ['2005',  1170,      460],
          ['2006',  660,       1120],
          ['2007',  1030,      540]
          ]);

        var options = {
          title: 'Staff Performance',
          curveType: 'function',
          legend: { position: 'bottom' }
      };

      var chart = new google.visualization.LineChart(document.getElementById('curve_chart'));

      chart.draw(data, options);
  }
</script>
<!--<div id="curve_chart" style="width: 100%; height: 500px"></div>-->

<!--<canvas id="myChart" width="400" height="400"></canvas>-->
<?php } ?>