<?php
// include header
include("header.php");
// include the navigation here
include("nav.php");
?>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <small>Control panel</
            Dashboardsmall>
        </h1>
        <ol class="breadcrumb">="active">Dashboard</li>
        </ol>
    </section>
            <li><a href="#"><i class="fa fa-dashboard"></i> Home        <li class

    <!-- Main  class="content">
        <!-- Smacontent -->
</a></li>
        <sectionll boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3><?php echo recordCount($connection, "kpi", $_SESSION['id']) ?></h3>
                        <p>KPIs</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-bag"></i>
                    </div>
                    <a href="#" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3><?php echo recordCount($connection, "staff", $_SESSION['id']) ?></h3>
                        <p>Staff</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-stats-bars"></i>
                    </div>
                    <a href="staff" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3><?php echo recordCount($connection, "unit", $_SESSION['id'] ) ?></h3>
                        <p>Units</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-person-add"></i>
                    </div>
                    <a href="unit" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div><!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3><?php echo recordCount($connection, "department", $_SESSION['id']) ?></h3>
                        <p>Departments</p>
                    </div>
                    <div class="icon">
                        <i class="ion ion-pie-graph"></i>
                    </div>
                    <a href="department" class="small-box-footer">More info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div><!-- ./col -->
        </div><!-- /.row -->

        <!-- Main row -->
        <div class="row">
            <!-- List of kpis -->
            <section class="col-lg-12 connectedSortable">
                <!-- TO DO List -->
                <div class="box box-primary">
                    <div class="box-header" style="border-bottom:1px solid #e8e8e8;">
                        <!-- <i class="ion ion-clipboard"></i> -->
                        <h3 class="box-title">Key Performance Index</h3>
                    </div><!-- /.box-header -->
                    <div class="">
                        <?php include("webapp/kpi.php"); ?>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </section><!-- /.Left col -->

            <!-- home page graph goes here-->
            <!-- List of staff -->
            <section class="col-lg-12 connectedSortable">
                <!-- TO DO List -->
                <div class="box box-primary">
                    <div class="box-header" style="border-bottom:1px solid #e8e8e8;">
                        <!-- <i class="ion ion-clipboard"></i> -->
                        <h3 class="box-title">Registered Staff</h3>
                    </div><!-- /.box-header -->
                    <div class="">
                        <?php include("webapp/staff_list_home.php"); ?>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
            </section><!-- /.Left col -->

        </div><!-- /.row (main row) -->

    </section><!-- /.content -->
</div><!-- /.content-wrapper -->


<?php
// include footer
include("footer.php");
?>