<!DOCTYPE html>
<html lang="en">
<head>
    
    <!-- start: Meta -->
    <meta charset="utf-8">
    <title>Login</title>
    <!-- end: Meta -->
    <!-- start: Mobile Specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- end: Mobile Specific -->
    <!-- start: CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
	<link href="css/style.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800&subset=latin,cyrillic-ext,latin-ext' rel='stylesheet' type='text/css'>
    <!-- end: CSS -->
    

    <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <link id="ie-style" href="css/ie.css" rel="stylesheet">
    <![endif]-->
    
    <!--[if IE 9]>
        <link id="ie9style" href="css/ie9.css" rel="stylesheet">
    <![endif]-->
        
    <!-- start: Favicon -->
    <link rel="shortcut icon" href="img/favicon.ico">
    <!-- end: Favicon -->
</head>

<body>
<header class="header">
<div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            <div class="account-wall">
				
				<form class="form-signin" name="frm_login" id="frm_login" action="login.php" method="">
					<h2>Login</h2>
					<?php echo md5("dimgba"); ?>
               		<input type="text" name="ad_name" id="username" class="form-control" placeholder="Email Address" required autofocus>
               		<input type="password" name="ad_pass" id="password" class="form-control" placeholder="Password" required>
					<div id="return_status_msg"></div>
               		<input type="submit" class="btn btn-lg btn-default btn-block" value="Sign In" id="btn_login" />
					<br />
					<label class="text-left remember" for="remember"><input type="checkbox" id="remember" /> Remember me</label>
					
					<!-- <p class="text-left"><a href="register">Click here to register or apply</a></p> -->
               	</form>
                
            </div>
        </div>
    </div>
</div>
</header>

 <!-- Placed at the end of the document so the pages load faster -->
<script src="js/jquery.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/validator.min.js"></script>
<script src="js/script.js"></script>
</body>
</html>

